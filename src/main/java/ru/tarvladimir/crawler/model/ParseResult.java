package ru.tarvladimir.crawler.model;

import java.util.Set;

/**
 * Рзультат обработки страницы
 *
 * Created by vladimir on 08.04.17.
 */
public class ParseResult {

    /** Статус обработки */
    private final ParseResultStatus status;
    /** Сообщение об ошибках */
    private final String message;
    /** Обработанная страница */
    private final String page;
    /** Список внутренних ссылок на странице */
    private final Set<String> innerLinks;
    /** Список внешних ссылок на странице*/
    private final Set<String> outerLinks;

    /**
     * Успешная обработка страницы
     * @param innerLinks список найденных внутренних ссылок
     * @param outerLinks список найденных внешних ссылок
     * @param page обработанная страница
     * @return результат обработки
     */
    public static ParseResult ok(Set<String> innerLinks, Set<String> outerLinks, String page) {
        return new ParseResult(innerLinks, outerLinks, page);
    }

    /**
     * Обработка страницы с ошибками, требующими повторную обработку
     * @param message сообщение об ошибке
     * @param page обработанная страница
     * @return результат обработки
     */
    public static ParseResult retry(String message, String page) {
        return new ParseResult(ParseResultStatus.RETRY, message, page);
    }

    /**
     * Фатальная ошибка при обработке странице, не требующая повторной обработки
     * @param message сообщение об ошибке
     * @param page    обработанная страница
     * @return результат обработки
     */
    public static ParseResult fail(String message, String page) {
        return new ParseResult(ParseResultStatus.FAILED, message, page);
    }

    private ParseResult(Set<String> innerLinks, Set<String> outerLinks, String page) {
        this(ParseResultStatus.OK, innerLinks, outerLinks, "Успешно", page);
    }

    private ParseResult(ParseResultStatus resultStatus, String message, String page) {
        this(resultStatus, null, null, message, page);
    }

    private ParseResult(ParseResultStatus status, Set<String> innerLinks, Set<String> outerLinks, String message, String page) {
        this.status = status;
        this.innerLinks = innerLinks;
        this.outerLinks = outerLinks;
        this.message = message;
        this.page = page;
    }

    public String getPage() {
        return page;
    }

    public String getMessage() {
        return message;
    }

    public ParseResultStatus getStatus() {
        return status;
    }

    public Set<String> getInnerLinks() {
        return innerLinks;
    }

    public Set<String> getOuterLinks() {
        return outerLinks;
    }

    @Override
    public String toString() {
        return "ParseResult{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", innerLinks=" + innerLinks +
                ", outerLinks=" + outerLinks +
                '}';
    }
}
