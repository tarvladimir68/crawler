package ru.tarvladimir.crawler.model;

/**
 * Статусы обработки страницы
 *
 * Created by vladimir on 08.04.17.
 */
public enum ParseResultStatus {
    /** Успешная обработка*/
    OK,
    /** Обработка с ошибками, требующая повторной обработки */
    RETRY,
    /** Обработка с фатальными ошибками не требующая повторной обработки */
    FAILED
}
