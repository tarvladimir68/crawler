package ru.tarvladimir.crawler.utils;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Набор вспомогательных методов
 *
 * Created by vladimir on 08.04.17.
 */
public class Utils {


    public static String normLink(String link) {
        return link.startsWith("https://") || link.startsWith("http://") ?
                link: "http://" + link;
    }

    public static String getSiteFromLink(String link) throws MalformedURLException {
        return new URL(link).getHost().replace("www.", "");
    }
}
