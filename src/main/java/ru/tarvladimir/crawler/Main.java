package ru.tarvladimir.crawler;

import ru.tarvladimir.crawler.parser.Crawler;
import ru.tarvladimir.crawler.utils.Utils;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Точка входа приложения
 *
 * Created by vladimir on 07.04.17.
 */
public class Main {

    /**
     * Основной запускающий метод программы
     *
     * @param args список аргументов запуска. Обязательное требуются следующий аргументы:
     *             1 - колличество потоков обработки данных
     *             2 - максимальное колличество внутренних ошибок сервера
     *             3 - максимальное колличество обрабатываемы страниц
     *                 (если передано -1 то будут обрабатываться все найденные страницы)
     *             4 - стартовая страница анализа
     */
    public static void main(String[] args) throws IOException, URISyntaxException {
        if (args == null || args.length != 4) {
            System.err.println("Не правильное колличество аргументов! " +
                    "Используйте запуск с параметрами " +
                    "<колличество потоков обработки> " +
                    "<максимальное колличество внутренних ошибок> " +
                    "<максимальное колличество обрабатываемых страниц>" +
                    "<стартовая страница> ");
            System.exit(1);
        }

        int countThread = getInt(args[0], "Колличество потоков обработки");
        int maxCountInternalError = getInt(args[1], "Максимальное число внутренних ошибок сервера");
        int maxCountLink = getInt(args[2], "Максимальное колличество обрабатываемых страниц");
        String startLink = Utils.normLink(args[3]);
        long startTime = System.currentTimeMillis();
        final String site = Utils.getSiteFromLink(startLink);
        System.out.println("Старт анализа. Анализируемый сайт " + site + " стартовая страница анализа " + startLink);
        int linkCount = new Crawler(countThread, maxCountInternalError, maxCountLink).run(startLink, site);
        System.out.println("Проанализировано " + linkCount + " страниц за время: " + ((System.currentTimeMillis() - startTime)) );
    }

    /**
     * Получить параметр типа int из входных значений
     * @param arg  входной параметр
     * @param name имя параметра
     * @return распарсенное значение параметра
     */
    private static int getInt(String arg, String name) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            System.err.println("Не верный формат: " + name);
            System.exit(1);
            return 0;
        }
    }

    /** Основной класс, запрещаем инстанцирование */
    private Main() {
    }
}
