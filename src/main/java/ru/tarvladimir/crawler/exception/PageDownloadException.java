package ru.tarvladimir.crawler.exception;

/**
 * Общий класс ошибок при загрузке страниц
 *
 * Created by vladimir on 08.04.17.
 */
public abstract class PageDownloadException extends Exception {

    /** http Код ответа сервера */
    private final int statusResponse;

    PageDownloadException(int statusResponse) {
        this.statusResponse = statusResponse;
    }

    public int getStatusResponse() {
        return statusResponse;
    }
}
