package ru.tarvladimir.crawler.exception;

/**
 * Ошибки возникающие при получении ответа сайта с 5xx ошибкой
 *
 * Created by vladimir on 08.04.17.
 */
public class InternalSiteException extends PageDownloadException {

    public InternalSiteException(int statusResponse) {
        super(statusResponse);
    }
}
