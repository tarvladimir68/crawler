package ru.tarvladimir.crawler.exception;

/**
 * Ошибки парсинга страницы
 *
 * Created by vladimir on 08.04.17.
 */
public class ParsePageException extends Exception {

    public ParsePageException(String message, Throwable cause) {
        super(message, cause);
    }
}
