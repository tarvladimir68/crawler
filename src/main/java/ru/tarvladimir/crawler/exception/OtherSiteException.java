package ru.tarvladimir.crawler.exception;

/**
 * Прочие ошибки при получении ответа от сайта
 *
 * Created by vladimir on 08.04.17.
 */
public class OtherSiteException extends Exception {

    public OtherSiteException(String message) {
        super(message);
    }

    public OtherSiteException(String message, Throwable cause) {
        super(message, cause);
    }
}
