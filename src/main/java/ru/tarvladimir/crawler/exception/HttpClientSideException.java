package ru.tarvladimir.crawler.exception;

/**
 * Исключение возникающее при 4xx ошибках
 *
 * Created by vladimir on 08.04.17.
 */
public class HttpClientSideException extends PageDownloadException {

    public HttpClientSideException(int statusResponse) {
        super(statusResponse);
    }
}
