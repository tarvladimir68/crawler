package ru.tarvladimir.crawler.parser;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClients;
import ru.tarvladimir.crawler.exception.InternalSiteException;
import ru.tarvladimir.crawler.exception.OtherSiteException;
import ru.tarvladimir.crawler.exception.HttpClientSideException;
import ru.tarvladimir.crawler.exception.ParsePageException;
import ru.tarvladimir.crawler.model.ParseResult;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Вспомогательный класс отвечающий за поиск ссылок на странице
 *
 * Created by vladimir on 07.04.17.
 */
class Parser {

    /** Регулярка для поиска сылок на страницах */
    private final static Pattern FIND_LINK = Pattern.compile("(<a.*?href=\")([^#]\\S*?)(\".*?>)");
    /** Регулярка для очистки страницы от тегов script */
    private final static Pattern REMOVE_SCRIPT =
            Pattern.compile("< *?script\\b[^<]*(?:(?!</script *?>)<[^<]*)*</script *?>");
    /** Регулярка для поиска стартовых слэшей в урле */
    private final static Pattern REMOVE_SLASHES = Pattern.compile("^/+");

    /**
     * Обработать страницу и найти на ней все ссылки
     * @param site анализируемый сайт
     * @param page анализируемая страница сайта
     * @return результат парсинга содержащий в себе списки внешних и внутренних ссылок
     */
    static ParseResult operate(String site, String page) throws IOException {
        try {
            Set<String> outLink = new HashSet<>();
            Set<String> innerLinks = new HashSet<>();
            for (String link: findLinks(site, page)) {
                if (isOuterLink(site, link)) {
                    outLink.add(link);
                } else {
                    innerLinks.add(link);
                }
            }
            return ParseResult.ok(innerLinks, outLink, page);
        } catch (InternalSiteException e) {
            return ParseResult.retry("Внутренния ошибка сервера: " + e.getStatusResponse(), page);
        } catch (HttpClientSideException e) {
            return ParseResult.fail("4xx ошибка при загрузки страницы: " + e.getStatusResponse(), page);
        } catch (OtherSiteException | ParsePageException e) {
            e.printStackTrace();
            return ParseResult.fail("Ошибка:" + e.getMessage(), page);
        } catch (Exception e) {
            e.printStackTrace();
            return ParseResult.fail("Не известная ошибка. " + e.getMessage(), page);
        }
    }

    /**
     * Проверить являеться ли ссылка внешне или внутренней
     * @param site анализируемый сайт
     * @param link ссылка на страницу
     * @return {@code true} ссылка внешняя, {@code false} ссылка является внутренней
     */
    private static boolean isOuterLink(String site, String link) throws MalformedURLException {
        try {
            return !new URL(link).getHost().contains(site);
        } catch (Exception e) {
            System.err.println("link" + link);
            throw e;
        }
    }

    /**
     * Разобрать страницу и найти на ней все ссылки
     * @param page разбираемая страница
     * @return список ссылок на странице
     */
    private static Set<String> findLinks(String site, String page)
            throws InternalSiteException, HttpClientSideException, OtherSiteException, ParsePageException {
        HttpClient httpClient = getHttpClient();
        HttpResponse response = getHttpResponse(page, httpClient);
        return handleResponse(site, page, response);
    }

    /**
     * Выполнить запрос на загрузку страницы
     * @param page ссылка на страницу
     * @param httpClient клиент для исполнения запросов
     * @return ответ на загрузку страницы
     */
    private static HttpResponse getHttpResponse(String page, HttpClient httpClient) throws OtherSiteException {
        HttpUriRequest request = new HttpGet(page);
        HttpResponse response;
        try {
            response = httpClient.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
            throw new OtherSiteException("Ошибка получения страницы " + page, e.getCause());
        }
        return response;
    }

    /**
     * Построить HttpClient
     * @return http клиент для загружки страниц
     */
    private static HttpClient getHttpClient() {
        // TODO если останеться время то проверить его на thread-safe и перенести в поле класса
        return HttpClients.custom()
                .disableCookieManagement()
                .build();
    }

    /**
     * Обработать ответ на загрузку страницы и распрасить содержимое страницы,
     * найти список всех ссылок на странице в тегах <a href="...."></a>
     *
     * @param site Анализируемый сайт
     * @param link Загружаемая страница сайта
     * @param response Ответ на загрузку страницы
     * @return список всех ссылок найденных на странице
     */
    private static Set<String> handleResponse(String site, String link, HttpResponse response)
            throws InternalSiteException, HttpClientSideException, ParsePageException, OtherSiteException {
        int status = response.getStatusLine().getStatusCode();
        if (status >= 500 && status < 600) {
            throw new InternalSiteException(status);
        } else if (status >= 400 && status < 500) {
            throw new HttpClientSideException(status);
        } else if (status >= 200 && status < 400) {
            try {
                return parse(site, preProcess(readPage(response.getEntity().getContent())));
            } catch (Exception e) {
                e.printStackTrace();
                throw new ParsePageException("Не удалось распарсить страницу: " + link + ". " + e.getMessage(), e);
            }
        } else {
            throw new OtherSiteException("Не обрабатываемый статус ответа: " + status + " при загрузке страницы:" + link);
        }
    }

    /**
     * Прочитать текст построчно из потока данных
     * @param inputStream входящий поток
     * @return считанные данные
     */
    private static String readPage(InputStream inputStream) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        }
    }

    /**
     * Произвести очистку страницы от тегов js, т.к. исполнять всё равно не можем,
     * а с ним затрудняется поиск ссылок на странице
     * @param page разбираемая страница
     * @return очищенная страница
     */
    private static String preProcess(String page) {
        return REMOVE_SCRIPT.matcher(page).replaceAll("");
    }

    /**
     * Метод разбора конкретной страницы, ищет все ссылки вида <a href=""/>
     *
     * @param page страница для разбора, важно! страница должна быть вычищена от тегов script
     *
     * @return список обнаруженных ссылок на странице
     */
    private static Set<String> parse(String site, String page) {
        Set<String> result = new HashSet<>();
        Matcher matcher = FIND_LINK.matcher(page);
        while (matcher.find()) {
            String link = matcher.group(2);
            if (!link.contains("javascript") && !link.contains("mailto:") && !link.contains("tel:")) {
                result.add(normLink(matcher.group(2), site));
            }
        }
        return result;
    }

    private static String normLink(String link, String site) {
        link = link.startsWith("/") ?
                REMOVE_SLASHES.matcher(link).replaceAll("") : link;

        link = link.startsWith("https://") || link.startsWith("http://") ?
                link : "http://" + (link.contains(site) ? link : site + "/" + link);
        return link;
    }

    /** Вспомогательный класс, запрещаем инстанцирование */
    private Parser() {
    }
}
