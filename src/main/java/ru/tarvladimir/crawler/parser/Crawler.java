package ru.tarvladimir.crawler.parser;

import ru.tarvladimir.crawler.model.ParseResult;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * Основной парсер сайта.
 *
 * Основной запускающий метод {@link Crawler#run(String, String)}.
 * Обрабатывает сайт начиная со стартовой страницы, в стандартный вывод
 * выводит найденные внешний ссылки и страницы на которых они были найдены.
 * Все ошибки выводяться в стандартный вывод.
 *
 * Не анализируются ссылки на страницах сгенрированные при помощи javascript, анализируются только ссылки в html
 *
 * Created by vladimir on 08.04.17.
 */
public class Crawler {


    /** Колличество потоков загрузки и парсинга страниц*/
    private final int countThread;
    /** Максимальное колличество внутренних ошибок после которого парсинг должен остановится */
    private final int maxCountInternalError;
    /** Максимальное колличество страниц сайта для обработки, если -1 то без ограничений **/
    private final int maxCountLink;

    /** Список обработанных страниц */
    private final Set<String> handlePage = new HashSet<>();
    /** Текущая очередь обработки */
    private final Queue<String> linksQueue = new LinkedList<>();
    /** Текущее колл-во внутренних ошибок сервера 5xx */
    private int internalErrorCount = 0;

    public Crawler(int countThread, int maxCountInternalError, int maxCountLink) {
        this.countThread = countThread;
        this.maxCountLink = maxCountLink;
        this.maxCountInternalError = maxCountInternalError;
    }

    /**
     * Запустить обработку сайта
     *
     * @param startLink стартовая страница для обработки
     * @param site      анализируемый сайт
     * @return колличество обработанных страниц сайта
     */
    public int run(String startLink, String site) {
        ExecutorService executorService = Executors.newFixedThreadPool(countThread);
        linksQueue.add(startLink);
        while (!linksQueue.isEmpty()) {
            if (checkMaxCountLink() || checkMaxInternalError()) {
                executorService.shutdownNow();
                return handlePage.size();
            }
            // Обработка страниц
            executeTasks(site, executorService).forEach(this::handleParseResult);
        }
        executorService.shutdownNow();
        return handlePage.size();
    }

    /**
     * Проверить на максимальное колличество внутренних ошибок сервера
     * @return {@code true} достигнуто максимальное колличество ошибок, необходимо остановить парсинг
     */
    private boolean checkMaxInternalError() {
        if (internalErrorCount > maxCountInternalError) {
            System.err.println("Внутренних ошибок сайта больше " + maxCountInternalError + " останавливаем парсинг");
            return true;
        }
        return false;
    }

    /**
     * Проверить на максимальное колличество обработанных страниц
     * @return {@code true} достигнуто максимальное колличество обрабатываемых страниц, необходимо остановаить парсинг
     */
    private boolean checkMaxCountLink() {
        if (maxCountLink != -1 && handlePage.size() > maxCountLink) {
            System.out.println("Достигнуто максимальное колличество обрабатываемых страниц: " +
                    handlePage.size() + " останавливаем парсинг");
            return true;
        }
        return false;
    }

    /**
     * Создать задачи на обработку сайта и добавить их в очередь на обработку
     * @param site анализируемый сайт
     * @return список выполняющихся задач для получения результата обработки
     */
    private List<Future<ParseResult>> executeTasks(String site, ExecutorService executorService) {
        List<Future<ParseResult>> futures = new ArrayList<>();
        for (int i = 0; i < countThread; i++) {
            String page = linksQueue.poll();
            if (page != null) {
                futures.add(executorService.submit(() -> Parser.operate(site, page)));
            }
        }
        return futures;
    }

    /**
     * Обработать результат разбора сайта
     * @param futureResult ссылка на результат обработки страницы
     */
    private void handleParseResult(Future<ParseResult> futureResult) {
        ParseResult parseResult;
        try {
            parseResult = futureResult.get();
        } catch (Exception e) {
            System.err.println("Ошибка обработки страницы " + e.getMessage());
            e.printStackTrace();
            return;
        }
        switch (parseResult.getStatus()) {
            case FAILED: handleFailResult(parseResult); break;
            case OK: handleSuccessResult(parseResult); break;
            case RETRY: handleRetryResult(parseResult); break;
        }
    }

    /**
     * Обработать парсинг страницы с фатальной ошибкой парсинга, не требующей повторной обработки
     * @param parseResult результат обработки страницы
     */
    private void handleFailResult(ParseResult parseResult) {
        System.err.println(parseResult.getMessage());
        handlePage.add(parseResult.getPage());
    }

    /**
     * Обработать результат парсинга страницы требующий повторной обработки (5xx ошибка)
     * @param parseResult результат обработки страницы
     */
    private void handleRetryResult(ParseResult parseResult) {
        internalErrorCount++;
        System.err.println("Внутренняя ошибка сайта, попытка загрузки страницы будет предпринята позже. Сообщение: " +
                parseResult.getMessage());
    }

    /**
     * Обработать результат успешного парсинга страницы
     * @param parseResult результат обработки страницы
     */
    private void handleSuccessResult(ParseResult parseResult) {
        handlePage.add(parseResult.getPage());
        for (String outLink : parseResult.getOuterLinks()) {
            System.out.println(parseResult.getPage() + " " + outLink);
        }
        getNeedOperateLink(parseResult.getInnerLinks()).forEach(this::addLinkToQueue);
    }

    /**
     * Поместить страницу сайта в очередь на обработку
     * @param str страница сайта
     */
    private void addLinkToQueue(String str) {
        if (!linksQueue.contains(str)) {
            linksQueue.add(str);
        }
    }

    /**
     * Получить список страниц необходимых для обработки
     *
     * @param links список новых страниц поступивших из парсера
     * @return список страниц для обработки
     */
    private Set<String> getNeedOperateLink(Set<String> links) {
        return links.stream()
                .filter(link -> !handlePage.contains(link))
                .filter(link -> {
                    link = link.toLowerCase();
                    return !(link.endsWith(".pdf") || link.endsWith(".jpg") || link.endsWith(".zip") || link.endsWith(".docx"));
                })
                .collect(Collectors.toSet());
    }
}
