#!/bin/bash
# до запуска приложения, отредактируйте параметра по необходимости
start_link="http://mail.ru"
thread_count=16
max_internal_error=50
max_count_link=100

java -classpath "./lib/*" ru.tarvladimir.crawler.Main ${thread_count} ${max_internal_error} ${max_count_link} ${start_link}