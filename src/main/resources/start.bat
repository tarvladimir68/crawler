@echo off
rem до запуска приложения, отредактируйте параметра по необходимости
set start_link="http://mail.ru"
set thread_count=16
set max_internal_error=50
set max_count_link=100

java -classpath "./lib/*" ru.tarvladimir.crawler.Main %thread_count% %max_internal_error% %max_count_link% %start_link%